version_info = (3, 0, 0, 'b1')
version = '3.0.0'
release = '3.0.0b1'

__version__ = release  # PEP 396
